require 'faye/websocket'
require 'redis'
require	'json'

module KittyChat
  class ChatBackend
    KEEPALIVE_TIME = 15 # in seconds
    CHANNEL        = "kitty-chat"

    def initialize(app)
      @app     = app
      @clients = []

      @redis   = Redis.new(host: "pub-redis-13464.us-east-1-1.2.ec2.garantiadata.com", port: 13464, password: "azertyuiop")
      Thread.new do
        redis_sub = Redis.new(host: "pub-redis-13464.us-east-1-1.2.ec2.garantiadata.com", port: 13464, password: "azertyuiop")
        redis_sub.subscribe(CHANNEL) do |on|
          on.message do |channel, msg|
            @clients.each {|ws| ws.send(msg) }
          end
        end
      end
    end

    def call(env)
    	if Faye::WebSocket.websocket?(env)
			ws = Faye::WebSocket.new(env, nil, {ping: KEEPALIVE_TIME })

			ws.on :open do |event|
				p [:open, ws.object_id]
				@clients << ws
				messages = @redis.zrevrangebyscore(CHANNEL, "+inf", "-inf", :limit => [0, 10])
				messages.reverse.each do |message|
					ws.send(message)
				end
			end

			ws.on :message do |event|
				p [:message, event.data]
				@redis.publish(CHANNEL, event.data)
				@redis.zadd(CHANNEL , Time.now.to_i, event.data)
			end

			ws.on :close do |event|
				p [:close, ws.object_id, event.code, event.reason]
				@clients.delete(ws)
				ws = nil
			end

			ws.rack_response
		else
			@app.call(env)
		end
    end
  end
end