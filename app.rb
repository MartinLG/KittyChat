require 'sinatra/base'

module KittyChat
  class App < Sinatra::Base
    get "/" do
      erb :"index.html"
    end
  end
end